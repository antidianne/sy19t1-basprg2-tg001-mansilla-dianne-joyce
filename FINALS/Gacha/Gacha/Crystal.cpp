#include "Crystal.h"
#include <iostream>
using namespace std;

Crystal::Crystal(string name, float chance, int addedCrystals) : Item(name, chance)
{
	mChance = chance; //0.15
	mAddedCrystals = addedCrystals;
}


Crystal::~Crystal()
{
}

void Crystal::getEffect(Player* player)
{
	Item::getEffect(player);
	cout << "Gained " << mAddedCrystals << " Crystals!" << endl;
	player->addCrystals(mAddedCrystals); //crystals += 15;
	player->addCrystalCount(1);
}