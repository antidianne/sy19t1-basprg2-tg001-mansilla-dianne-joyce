#include <iostream>
#include <string>
using namespace std;

int gold = 250;
int packages[] = { 30750, 1780, 500, 250000, 13333, 150, 4050 };

void sort(int packages[])
{
	int store;
	for (int i = 0; i < 7; i++)
	{
		for (int x = i + 1; x < 7; x++)
		{
			if (packages[i] > packages[x])
			{
				store = packages[i];
				packages[i] = packages[x];
				packages[x] = store;
			}
		}
		//cout << packages[i] << ", ";
	}
	
}

void suggest(int packages[], int input)
{
	string purch, another;
	int a, tempmoney;
	bool found = false;

	if (packages[6] <  input)
		{
			cout << "There is no available package to buy the item. Do you want to choose another item? " << endl;
			
		}
	else
	{
		
		for (int i = 0; i < 7; i++)
		{
			tempmoney = gold + packages[i];
			if (tempmoney >= input)
			{
				found = true;
				a = i;
				break;
			}
		}
		if (found)
		{
			cout << "Do you want to purchase " << packages[a] << " ? (Y/N)" << endl;
			cin >> purch;

			if (purch == "y" || purch == "Y")
			{
				gold = gold + packages[a];
				cout << "Purchase complete! Your current gold is: " << gold << endl;
			}

			else
			{
				cout << "Transaction cancelled. Do you want to choose another item?" << endl;
				
			}
			
		}
	}
	system("pause");
	system("CLS");
}

int main()
{
	int input = 0;
	string purch;
	do {
		cout << "Current gold: " << gold << endl;
		cout << "Price of item you want to buy?" << endl;
		cin >> input;

		//case 1
		if (gold > input)
		{
			cout << " You have enough gold to make this purchase. Do you want to continue? (Y/N)" << endl;
			cin >> purch;

			if (purch == "y" || purch == "Y")
			{
				gold = gold - input;
				cout << "Purchase complete! Your current gold is: " << gold << endl;
			}
			else
			{
				cout << "Transaction cancelled." << endl;
			}
			system("pause");
			system("CLS");
		}
		//case 2
		else
		{
			cout << "You do not have enough gold to make this purchase." << endl;
			sort(packages);
			suggest(packages, input);
		}
	} while (1);

	system("pause");
	return 0;
}