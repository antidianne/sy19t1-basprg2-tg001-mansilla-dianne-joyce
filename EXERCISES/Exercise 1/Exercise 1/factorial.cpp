#include <iostream>
#include <string>

using namespace std;

int factor(int number)
{
	int y = 1;

	if (number == 0)
	{
		y = 1;
	}
	else
	{
		for (int i = 0; i < number; i++)
		{
			y = y * (i + 1);

		}
	}
	return y;
}

int main() {

	int x;

	cout << "Input number: " << endl;
	cin >> x;

	cout << "Factorial: " << factor(x) << endl;

	system("pause");
}