#pragma once
#include "Item.h"
#include <string>
#include <iostream>
using namespace std;

class HealthPotion : public Item
{
public:
	HealthPotion(string name, float chance, int healValue);
	~HealthPotion();

	void getEffect(Player* player);

private:
	float mChance;
	int mHealValue;
};
