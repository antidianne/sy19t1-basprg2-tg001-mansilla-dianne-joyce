#pragma once
#include "Item.h"
#include <string>
#include <iostream>
using namespace std;

class R : public Item
{
public:
	R(string name, float chance, int addedRarityPoints);
	~R();

	void getEffect(Player* player);

private:
	float mChance;
	int mAddedRarityPoints;
};
