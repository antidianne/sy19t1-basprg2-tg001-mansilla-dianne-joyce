#pragma once
#include "Item.h";
#include <string>
#include <iostream>
using namespace std;

class SSR : public Item
{
public:
	SSR(string name, float chance, int addedRarityPoints);
	~SSR();

	void getEffect(Player* player);

private:
	float mChance;
	int mAddedRarityPoints;
};