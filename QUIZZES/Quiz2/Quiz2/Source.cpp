//#include "Node.h"
#pragma once
#ifndef NODE_H
#define NODE_H
#include <string>

struct Node
{
	std::string name;
	Node* next = NULL;
	Node* previous = NULL;
};

#endif //NODE_H

#include <string>
#include <iostream>
#include <time.h>

using namespace std;

void naming()
{
	string name;
	cout << "enter soldier's name: " << endl;
	for (int i = 0; i < 5; i++) {

		cout << "Soldier " << i + 1 << ": ";
		cin >> name;
	}
}

Node* linklist()
{
	Node* n1 = new Node;
	Node* n2 = new Node;
	Node* n3 = new Node;
	Node* n4 = new Node;
	Node* n5 = new Node;

	n1->next = n2;
	n2->next = n3;
	n3->next = n4;
	n4->next = n5;
	n5->next = n1;

	n1->name;
	n2->name;
	n3->name;
	n4->name;
	n5->name;

	n1->previous = n5;
	n2->previous = n1;
	n3->previous = n2;
	n4->previous = n3;
	n5->previous = n4;

	// Iterate or loop through the nodes
	Node* current = n1;
	Node* head = current;
	return head;
}
//printing soldier names
void printList(Node* head, int soldierCount)
{
	for (int i = 0; i < soldierCount; i++)
	{
		cout << head->name << endl;
		head = head->next;
	}
}

//passing cloak to random soldier
Node* cloakPass(Node* head, int random)
{
	for (int i = 0; i < random; i++)
	{
		head = head->next;
	}

	return head;
}

// deleting node of chosen sol
Node* deleteSoldier(Node* head, int random)
{
	Node* previous = NULL;
	Node* deleteNode = head;

	//loops till "deleteNode" connects to the node that should be removed
	//previous connects to tail of deleteNode
	for (int i = 0; i < random; i++)
	{
		previous = deleteNode;
		deleteNode = deleteNode->next;
	}

	//connects previous to next node of deleteNode
	previous->next = deleteNode->next;

	head = deleteNode->next;
	head->previous = previous;

	delete deleteNode;

	return head;
}

int main()
{
	srand(time(0));

	Node* head = linklist();

	int soldierCount = 5;
	int round = 1;

	//loop till only 1 soldier remains
	while (soldierCount > 1)
	{
		cout << "Round: " << round << endl;
		cout << "Remaining members: " << endl;

		printList(head, soldierCount);

		int chosenSoldier = rand() % soldierCount + 1;

		cout << endl;
		cout << "Result: " << endl;
		cout << head->name << " has drawn " << chosenSoldier << endl;

		Node* cloakHolder = cloakPass(head, chosenSoldier);

		cout << cloakHolder->name << " has been eliminated" << endl;
		cout << endl;

		//call delete function to delete current head
		head = deleteSoldier(head, chosenSoldier);

		//add round and decrease soldiercount
		round++;
		soldierCount--;

		system("pause");
	}

	//final result
	cout << "FINAL RESULT" << endl;
	cout << head->name << " will go to seek for reinforcements." << endl;

	system("pause");
	return 0;
}