#pragma once
#include "Item.h"
#include <string>
#include <iostream>
using namespace std;

class Crystal : public Item
{
public:
	Crystal(string name, float chance, int addedCrystals);
	~Crystal();

	void getEffect(Player* player);

private:
	float mChance;
	int mAddedCrystals;
};