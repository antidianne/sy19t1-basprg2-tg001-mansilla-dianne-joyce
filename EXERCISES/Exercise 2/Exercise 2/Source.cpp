#include <iostream>
#include <conio.h>
#include <time.h>
using namespace std;

struct roll
{
	int dice[2];
	int value;
}

player, dealer;

//2-1
int betting(int& gold)
{
	int bet;
	cout << "Current gold: " << gold << endl;
	cout << "Enter bet:" << endl;
	cin >> bet;

	if ((bet != 0) && (bet > gold))
	{
		gold = gold - bet;
	}
	else
	{
		cout << "The bet you entered is invalid." << endl;
	}

	return bet;
}

void betting(int& gold, int& bet)
{
	cout << "Current gold: " << gold << endl;
	cout << "Enter bet:" << endl;
	cin >> bet;

	if ((bet != 0) && (bet > gold))
	{
		gold = gold - bet;
	}
	else
	{
		cout << "The bet you entered is invalid." << endl;
	}
}
//2-2
void rollDice(roll& roller)
{
	for (int i = 0; i < 2; i++) {
		roller.dice[i] = rand() % 6 + 1;

		cout << "DICE " << i + 1 << ": " << roller.dice[i] << endl;
	}
	roller.value = roller.dice[0] + roller.dice[1];
	cout << "VALUE: " << roller.value << endl;
}

//EXERCISE 2-3
void payout(roll player, roll dealer, int bet, int& gold)
{
	int pay = 0;
	cout << endl;

	//SAME DICE
	if (player.value == dealer.value)
	{
		pay = 0;
		cout << "IT'S A DRAW" << endl;
	}

	// SNAKE EYES
	else if (player.value == 2)
	{
		pay = bet * 3;
		cout << "PLAYER WON" << endl;
	}

	//DEALER HIGHER DICE
	else if ((player.value < dealer.value) || (dealer.value == 2))
	{
		pay = gold - bet;
		cout << "DEALER WINS" << endl;
	}

	//PLAYER HIGHER DICE 
	else if (player.value > dealer.value)
	{
		pay = bet;
		cout << "PLAYER WINS" << endl;
	}

	cout << endl;
	cout << "PAYOUT:\t" << pay << endl;
	//PAYOUT WILL BE ADDED TO THE GOLD
	gold = gold + bet + pay;
	cout << "GOLD:\t" << gold << endl;
}

//EXERCISE 2-4
void playRound(int& gold)
{
	int bet;

	system("cls");
	bet = betting(gold);
	system("cls");

	//PLAYER WILL ROLL DICE
	cout << "PLAYER DICE" << endl;
	rollDice(player);
	//DEALER WILL ROLL DICE
	cout << "\nDEALER DICE" << endl;
	rollDice(dealer);
	//COMPUTES PAYOUT IF WIN or LOSE
	payout(player, dealer, bet, gold);
}

int main()
{
	srand(time(0));
	int gold = 1000;
	char playAgain;

	do {
		system("cls");
		playRound(gold);
		cout << "Press any key to play again or press \"x\" to quit. " << endl;
		playAgain = _getch();
	} while ((gold > 0) && (playAgain != 'x'));

	system("cls");
	if (gold <= 0) { cout << "YOU HAVE NO MORE GOLD. GAME OVER" << endl; }
	cout << "THANK YOU FOR PLAYING!" << endl;

	system("pause");
	return 0;
}

