#pragma once
#include <string>
#include <iostream>
#include "Player.h"
using namespace std;

class Item
{
public:
	Item();
	Item(string name, float chance);
	~Item();

	string getName();
	float getChance();

	virtual void getEffect(Player* player);

protected:
	string mName;
	float mChance;
};