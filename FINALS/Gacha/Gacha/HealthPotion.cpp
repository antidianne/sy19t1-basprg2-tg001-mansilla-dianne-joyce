#include "HealthPotion.h"
#include <iostream>
using namespace std;

HealthPotion::HealthPotion(string name, float chance, int healValue) : Item(name, chance)
{
	mChance = chance; //0.15
	mHealValue = healValue;
}


HealthPotion::~HealthPotion()
{
}

void HealthPotion::getEffect(Player* player)
{
	Item::getEffect(player);
	cout << "Healed for " << mHealValue << " points." << endl;
	player->changeHp(mHealValue); //hp += 30;
	player->addHpCount(1);
}