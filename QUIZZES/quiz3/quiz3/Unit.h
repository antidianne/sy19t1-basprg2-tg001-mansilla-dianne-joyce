#pragma once
#include <string>
using namespace std;

class Unit
{
public:
	Unit();
	Unit(string name, string userClass);
	~Unit();

	string getName();
	string getClass();
	int getHp();
	int getMaxHp();
	int getPow();
	int getVit();
	int getAgi();
	int getDex();

	void printStats();
	void takeDamage(int damage);
	void recoverHealth();
	void addStat(string eClass);
	bool faster(int pAgi, int eAgi);
	bool hitRate(int attDex, int defAgi);
	int computeDamage(string attClass, string defClass);

private:
	string mName;
	string mClass;
	int mHp;
	int mMaxHp;
	int mPow;
	int mVit;
	int mAgi;
	int mDex;
};