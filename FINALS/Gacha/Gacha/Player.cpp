#include "Player.h"
#include "Item.h"
#include <iostream>
#include <string>
using namespace std;


Player::Player() //Default in case of wanting another player
{
	mName = "Player";
	mHp = 100;
	mCrystals = 100;
	mRarityPoints = 0;
}

Player::Player(string name, int hp, int crystals, int rarityPoints) //For wanting to input the stats
{
	mName = name;
	mHp = hp;
	mCrystals = crystals;
	mRarityPoints = rarityPoints;
}


Player::~Player()
{
	//When you delete player, the items in the gacha also get deleted
	/*for (int x = 0; x < items.size(); x++)
	{
	delete items[x];
	}*/
}

string Player::getName()
{
	return mName;
}

int Player::getHp()
{
	return mHp;
}

int Player::getCrystals()
{
	return mCrystals;
}

int Player::getRarityPoints()
{
	return mRarityPoints;
}

void Player::changeHp(int amount)
{
	mHp += amount;
}

void Player::addCrystals(int amount)
{
	mCrystals += amount;
}

void Player::addRarityPoints(int amount)
{
	mRarityPoints += amount;
}

void Player::addBombCount(int count)
{
	mBombCounter += count;
}

void Player::addCrystalCount(int count)
{
	mCrystalCounter += count;
}

void Player::addHpCount(int count)
{
	mHealthPotionCounter += count;
}

void Player::addRCount(int count)
{
	mRCounter += count;
}

void Player::addSrCount(int count)
{
	mSrCounter += count;
}

void Player::addSsrCount(int count)
{
	mSsrCounter += count;
}

int Player::getBombCounter()
{
	return mBombCounter;
}

int Player::getCrystalCounter()
{
	return mCrystalCounter;
}

int Player::getHealthPotionCounter()
{
	return mHealthPotionCounter;
}

int Player::getRCounter()
{
	return mRCounter;
}

int Player::getSrCounter()
{
	return mSrCounter;
}

int Player::getSsrCounter()
{
	return mSsrCounter;
}

void Player::setItemCounters(int bombcount, int crystalcount, int hpcount, int rcount, int srcount, int ssrcount)
{
	mBombCounter = bombcount;
	mCrystalCounter = crystalcount;
	mHealthPotionCounter = hpcount;
	mRCounter = rcount;
	mSrCounter = srcount;
	mSsrCounter = ssrcount;
}

void Player::viewPlayerStats(int pulls)
{
	if (mHp < 0) mHp = 0;
	cout << "Name: " << mName << endl;
	cout << "HP: " << mHp << endl;
	cout << "Crystals: " << mCrystals << endl; //free pull = mCrystals % 5
	cout << "Rarity Points: " << mRarityPoints << endl;
	cout << "Pulls: " << pulls << endl;
}

void Player::viewPulledItems()
{
	cout << "=====================" << endl << "ITEMS PULLED:" << endl;
	cout << "SSR: " << mSsrCounter << endl;
	cout << "SR: " << mSrCounter << endl;
	cout << "R: " << mRCounter << endl;
	cout << "Health Potion: " << mHealthPotionCounter << endl;
	cout << "Bomb: " << mBombCounter << endl;
	cout << "Crystals: " << mCrystalCounter << endl;
}

void Player::viewEnding(Player* player)
{
	if (player->getHp() <= 0) {
		cout << "You have lost because your HP is depleted!" << endl;
	}
	else if (player->getCrystals() <= 0) {
		cout << "You have lost because you have no more crystals left for a gacha pull!" << endl;
	}
	else if (player->getRarityPoints() >= 100) {
		cout << "Congratulations! You have gained 100 rarity points! You win!" << endl;
	}
}