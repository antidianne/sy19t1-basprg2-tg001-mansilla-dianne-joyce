#include "Bomb.h"
#include <iostream>
using namespace std;

Bomb::Bomb(string name, float chance, int damageValue) : Item(name, chance)
{
	mChance = chance; //0.20
	mDamageValue = damageValue;
}

Bomb::~Bomb()
{
}

void Bomb::getEffect(Player* player)
{
	Item::getEffect(player);
	cout << "Damaged by " << mDamageValue * -1 << " points" << endl;
	player->changeHp(mDamageValue); //-25
	player->addBombCount(1);
}