#pragma once
#include <string>
#include <iostream>
#include <cstdlib>
#include <vector>
using namespace std;

class Item;

class Player
{
public:
	Player();
	Player(string name, int hp, int crystals, int rarityPoints);
	~Player();

	//Getters
	string getName();
	int getHp();
	int getCrystals();
	int getRarityPoints();

	//Stats updated from the Items Pulled
	void changeHp(int amount);
	void addCrystals(int amount);
	void addRarityPoints(int amount);
	void addBombCount(int count);
	void addCrystalCount(int count);
	void addHpCount(int count);
	void addRCount(int count);
	void addSrCount(int count);
	void addSsrCount(int count);

	//Item Counter
	int getBombCounter();
	int getCrystalCounter();
	int getHealthPotionCounter();
	int getRCounter();
	int getSrCounter();
	int getSsrCounter();
	void setItemCounters(int bombcount, int crystalcount, int hpcount, int rcount, int srcount, int ssrcount);

	//Stat viewing
	void viewPlayerStats(int pulls);
	void viewPulledItems();
	void viewEnding(Player* player);

private:
	string mName;
	int mHp;
	int mCrystals;
	int mRarityPoints;
	int mBombCounter;
	int mCrystalCounter;
	int mHealthPotionCounter;
	int mRCounter;
	int mSrCounter;
	int mSsrCounter;
	//vector<Item*> mPullItems;
};
