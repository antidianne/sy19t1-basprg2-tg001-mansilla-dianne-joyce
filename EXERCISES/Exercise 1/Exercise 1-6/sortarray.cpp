#include <iostream>

using namespace std;
int arr[10];

void randomArray(int arr[], int size)
{
	for (int i = 0; i < size; i++)
	{
		arr[i] = rand() % 100 + 1;
	}
}

void print(int arr[], int size)
{
	for (int i = 0; i < size; i++)
	{
		cout << arr[i] << ", ";
	}
}

void sorting(int arr[], int size)
{
	int temp;

	for (int i = 0; i < size; i++)
	{
		for (int x = i + 1; x < size; x++)
		{
			if(arr[i] < arr[x])
			{
				temp = arr[i];
				arr[i] = arr[x];
				arr[x] = temp;
			}
		}
		cout << arr[i] << ", ";
	}

}


int main()
{
	int size;
	cout << "enter number of elements:" << endl;
	cin >> size;

	randomArray(arr,size);
	print(arr, size);
	cout << endl;
	sorting(arr, size);
	

	system("pause");
	return 0;
}