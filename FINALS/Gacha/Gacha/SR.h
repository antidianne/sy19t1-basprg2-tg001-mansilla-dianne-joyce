#pragma once
#include "Item.h"
#include <string>
#include <iostream>
using namespace std;

class SR : public Item
{
public:
	SR(string name, float chance, int addedRarityPoints);
	~SR();

	void getEffect(Player* player);

private:
	float mChance;
	int mAddedRarityPoints;
};