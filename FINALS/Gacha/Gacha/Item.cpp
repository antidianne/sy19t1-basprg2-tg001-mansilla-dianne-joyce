#include "Item.h"


Item::Item()
{
}

Item::Item(string name, float chance)
{
	mName = name;
	mChance = chance;
}


Item::~Item()
{
}

string Item::getName()
{
	return mName;
}

float Item::getChance()
{
	return mChance;
}

void Item::getEffect(Player* player)
{
	cout << player->getName() << " has pulled a " << mName << "!" << endl;
	player->addCrystals(-5);
}