#include "SSR.h"
#include "Item.h"
#include <iostream>
using namespace std;

SSR::SSR(string name, float chance, int addedRarityPoints) : Item(name, chance)
{
	mChance = chance; //0.01
	mAddedRarityPoints = addedRarityPoints;
}


SSR::~SSR()
{
}

void SSR::getEffect(Player* player)
{
	Item::getEffect(player);
	cout << "Gained " << mAddedRarityPoints << " rarity points." << endl;
	player->addRarityPoints(mAddedRarityPoints); //rarityPoints += 50;
	player->addSsrCount(1);
}