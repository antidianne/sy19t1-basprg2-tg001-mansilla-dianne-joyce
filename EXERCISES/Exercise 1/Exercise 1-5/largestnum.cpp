#include <iostream>

using namespace std;
int arr[10];

void randomArray(int arr[])
{
	for (int i = 0; i < 10; i++)
	{
		arr[i] = rand() % 100 + 1;
	}
}

void print(int arr[])
{
	for (int i = 0; i < 10; i++)
	{
		cout << arr[i] << ", ";
	}
}

void larger(int arr[])
{
	int large = arr[0];

	for (int i = 0; i < 10; i++) 
	{
		if (large < arr[i])
		{
			large = arr[i];
		}

	}
		
	cout << "Largest number : " << large << endl;
}

int main()
{
	randomArray(arr);
	print(arr);
	cout << endl;
	larger(arr);

	system("pause");
	return 0;
}