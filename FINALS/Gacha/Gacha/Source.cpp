#include <iostream>
#include <vector>
#include <cstdlib>
#include <ctime>
#include <string>
#include <time.h>
#include "Player.h"
#include "SSR.h"
#include "SR.h"
#include "R.h"
#include "HealthPotion.h"
#include "Bomb.h"
#include "Crystal.h"
using namespace std;

int main()
{
	srand(time(NULL));

	//Set the chance percentage of items and their amount of effect
	SSR* ssr = new SSR("SSR", 1, 50);
	SR* sr = new SR("SR", 9, 10);
	R* r = new R("R", 40, 1);
	HealthPotion* healthPotion = new HealthPotion("Health Potion", 15, 30);
	Bomb* bomb = new Bomb("Bomb", 20, -25);
	Crystal* crystal = new Crystal("Crystal", 15, 15);

	//Add all the items into a vector array for randomizing
	vector <Item*> items;
	items.push_back(ssr);
	items.push_back(sr);
	items.push_back(r);
	items.push_back(healthPotion);
	items.push_back(bomb);
	items.push_back(crystal);


	system("pause");
	return 0;
}