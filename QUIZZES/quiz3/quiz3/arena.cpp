#include "Unit.h"
#include <iostream>
#include <string>
#include <ctime>
using namespace std;

//Class Pick.
void pickClass(string userClass)
{
	int userChoice = 0;
	do
	{
		cout << "Warrior  -1" << endl;
		cout << "Assassin -2" << endl;
		cout << "Mage     -3" << endl;
		cout << "Choose your Class: ";
		cin >> userChoice;

		if (userChoice == 1)
		{
			cout << "You have chosen Warrior class" << endl;
			userClass == "Warrior";
		}
		else if (userChoice == 2)
		{
			cout << "You have chosen Assassin class" << endl;
			userClass == "Assassin";
		}
		else if (userChoice == 3)
		{
			cout << "You have chosen Mage class" << endl;
			userClass == "Mage";
		}

	} while (userChoice >= 4 || userChoice == 0);
	system("CLS");
}

void createEnemy(Unit* enemy)
{
	enemy->printStats();
	cout << endl;

	system("pause");
	system("CLS");
}

//if player agi is higher = true
//transferred to unit.h
/*bool faster(Unit *player, Unit *enemy)
{
	if (player->getAgi() >= enemy->getAgi())
	{
		return true;
	}
	else
	{
		return false;
	}
}*/

//if hitRate is greater than the randomized variable, hit will succeed
//transferred to unit.h
/*bool hitRate(Unit *attacker, Unit *defender)
{
	int randomizeHit = rand() % 100 + 1;
	int hitRate = (attacker->getDex() / defender->getAgi()) * 100;

	if (hitRate <= 20)
	{
		hitRate = 20;
	}
	else if (hitRate >= 80)
	{
		hitRate = 80;
	}

	if (hitRate >= randomizeHit)
	{
		return true;
	}
	else
	{
		return false;
	}
}*/

//incomplete
/*float computeDamage(Unit *attacker, Unit *defender)
{
	float bonusDamage = 0;

	if (attacker->getClass() == "Warrior" && defender->getClass() == "Assassin"
		|| attacker->getClass() == "Assassin" && defender->getClass() == "Mage"
		|| attacker->getClass() == "Mage" && defender->getClass() == "Warrior")
	{
		bonusDamage = 2.0f;
	}
	else if (attacker->getClass() == defender->getClass())
	{
		bonusDamage = 1.0f;
	}
	else if(attacker->getClass() == "Warrior" && defender->getClass() == "Mage"
		|| attacker->getClass() == "Assassin" && defender->getClass() == "Warrior"
		|| attacker->getClass() == "Mage" && defender->getClass() == "Assassin")
	{
		bonusDamage = 0.5;
	}

	return 5;
}*/

//if firstAttack is true, player hits first
void combat(Unit* player, Unit* enemy, bool& firstAttack, bool& bothAlive)
{
	int damage;

	cout << player->getName() << " HP: " << player->getHp() << endl;
	cout << enemy->getName() << " HP: " << enemy->getHp() << endl;
	cout << endl;

	if (firstAttack == true)
	{
		bool playerHit = player->hitRate(player->getDex(), enemy->getAgi());
		if (playerHit == true)
		{
			damage = player->computeDamage(player->getClass(), enemy->getClass());
			enemy->takeDamage(damage);
			cout << player->getName() << " gave " << damage << " damage!" << endl;
		}
		else if (playerHit == false)
		{
			cout << player->getName() << " missed his/her attack!" << endl;
		}
		//enemy attacks after player
		if (enemy->getHp() > 0)
		{

			bool enemyHit = enemy->hitRate(enemy->getDex(), player->getAgi());
			if (enemyHit == true)
			{
				damage = enemy->computeDamage(enemy->getClass(), player->getClass());
				player->takeDamage(damage);
				cout << enemy->getName() << " gave " << damage << " damage!" << endl;;
			}
			else if (enemyHit == false)
			{
				cout << enemy->getName() << " missed his/her attack!" << endl;
			}
		}

	}

	else if (firstAttack == false)
	{
		bool enemyHit = enemy->hitRate(enemy->getDex(), player->getAgi());
		if (enemyHit == true)
		{
			damage = enemy->computeDamage(enemy->getClass(), player->getClass());
			player->takeDamage(damage);
			cout << enemy->getName() << " gave " << damage << " damage!" << endl;;
		}
		else if (enemyHit == false)
		{
			cout << enemy->getName() << " missed his/her attack!" << endl;
		}
		//player attacks after enemy
		if (player->getHp() > 0)
		{
			bool playerHit = player->hitRate(enemy->getDex(), player->getAgi());
			if (playerHit == true)
			{
				damage = player->computeDamage(player->getClass(), enemy->getClass());
				enemy->takeDamage(damage);
				cout << player->getName() << " gave " << damage << " damage!" << endl;
			}
			else if (playerHit == false)
			{
				cout << player->getName() << " missed his/her attack!" << endl;
			}
		}
	}
	system("pause");
	system("CLS");

	if (enemy->getHp() == 0 || player->getHp() == 0)
	{
		bothAlive = false;
	}
}

//end game
void lose(int round, Unit* player)
{
	cout << player->getName() << " lasted for " << round << " rounds" << endl;
	cout << "Stats on last round: " << endl;
	player->printStats();

	system("pause");
}

void playRound(Unit* player)
{
	int round = 0;
	do
	{
		round++;
		cout << "Round: " << round << endl;
		cout << endl;
		player->printStats();
		cout << endl;
		string classes[3] = { "Warrior", "Assassin", "Mage" };
		int randomize = rand() % 3;
		Unit* enemy = new Unit(classes[randomize], classes[randomize]);

		createEnemy(enemy);
		//if one dies, bothAlive loop breaks.
		bool bothAlive = true;
		do
		{
			//if firstAttack is true, player attacks first.
			bool firstAttack = player->faster(player->getAgi(), enemy->getAgi());
			combat(player, enemy, firstAttack, bothAlive);
		} while (bothAlive == true);
		//checks to see if player is the one who died.
		//if player hp > 1, main loop does not break.
		//if player hp < 1, break.
		if (player->getHp() < 1)
		{
			break;
		}
		//add stats depending on enemy class
		player->addStat(enemy->getClass());
		player->recoverHealth();
		delete enemy;
	} while (player->getHp() > 0);

	system("CLS");
	lose(round, player);
}

int main()
{
	srand(time(0));
	string name;
	string userClass;

	cout << "Enter Name: ";
	cin >> name;
	cout << endl;
	pickClass(userClass);

	Unit* player = new Unit(name, userClass);

	playRound(player);

	delete player;
}