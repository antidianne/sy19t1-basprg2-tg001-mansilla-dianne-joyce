#include "SR.h"
#include "Item.h"
#include <iostream>
using namespace std;

SR::SR(string name, float chance, int addedRarityPoints) : Item(name, chance)
{
	mChance = chance; //0.09
	mAddedRarityPoints = addedRarityPoints;
}


SR::~SR()
{
}

void SR::getEffect(Player* player)
{
	Item::getEffect(player);
	cout << "Gained " << mAddedRarityPoints << " rarity points." << endl;
	player->addRarityPoints(mAddedRarityPoints); //rarityPoints += 10;
	player->addSrCount(1);
}
