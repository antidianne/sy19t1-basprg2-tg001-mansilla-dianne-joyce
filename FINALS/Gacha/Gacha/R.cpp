#include "R.h"
#include "Item.h"
#include <iostream>
using namespace std;

R::R(string name, float chance, int addedRarityPoints) : Item(name, chance)
{
	mChance = chance; //0.40
	mAddedRarityPoints = addedRarityPoints;
}


R::~R()
{
}

void R::getEffect(Player* player)
{
	Item::getEffect(player);
	cout << "Gained " << mAddedRarityPoints << " rarity points." << endl;
	player->addRarityPoints(mAddedRarityPoints);
	player->addRCount(1);
}