#include <iostream>
#include <time.h>
#include <conio.h>
using namespace std;

struct Item
{
	string name;
	int gold;
}; lootedItem;

//3-4
item randomItem()
{
	item loot;
	string itemName[5] = { "Mithril Ore", "Sharp Talon", "Thick Leather" , "Jellopy", "Cursed Stone" };
	int itemPrice[5] = { 100, 50, 25, 5, 0 };
	int random;

	random = rand() % 5;
	loot.name = itemName[random];
	loot.gold = itemPrice[random];

	return loot;
}

//3-5
void enterDungeon(int& gold) {
	char cont;
	int totalLootGold = 0, i = 1;
	gold = gold - 25;

	do {
		system("cls");
		lootedItem = generateItem();

		if (lootedItem.name == "Cursed Stone") {
			cout << "You looted a Cursed stone! You will lose all your looted gold and will exit the dungeon now." << endl;
			system("pause");
			totalLootGold = 0; break;
		}
		cout << "You looted a " << lootedItem.name
			<< " worth " << lootedItem.gold << " (x" << i << ')' << endl;

		totalLootGold = totalLootGold + (lootedItem.gold * i);
		cout << "Total looted gold:\t" << totalLootGold << endl;

		cout << "\nPress any key to continue looting or press \"x\" to exit dungeon";
		cont = _getch();

		i++;
		if (i >= 4) { i = 4; }

	} while (cont != 'x');

	gold = gold + totalLootGold;
}

//3-6
int main() {
	srand(time(NULL));
	int playerGold = 50;

	while ((playerGold >= 25) && (playerGold < 500)) {
		system("cls");

		cout << "\n\nPRESS ANY KEY TO ENTER THE DUNGEON. YOUR TOTAL GOLD:\t" << playerGold; _getch();
		enterDungeon(playerGold);
	};

	system("cls");

	if (playerGold >= 500) {
		cout << "LIMIT ACHIEVED! YOU HAVE " << playerGold << " GOLD. THANK YOU FOR PLAYING!" << endl;
	}
	else if (playerGold <= 0) { cout << "YOU DON'T HAVE GOLD ANY MORE! GAME OVER!!!\n" << endl; }
	else { cout << "YOU DON'T HAVE ANY MORE GOLD TO ENTER THE DUNGEON. GAME OVER!!!\n" << endl; }

	system("pause");
	return 0;
}
