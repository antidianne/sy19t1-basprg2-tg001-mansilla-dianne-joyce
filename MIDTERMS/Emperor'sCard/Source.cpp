#include <iostream>
#include <time.h>
#include <cstdlib>
#include <string>
#include <vector>
using namespace std;

//constant minimum bet and goal
const int minimum = 1;
const int goal = 20000000;

//betting
void wager(int perica, int& bet, int& distance)
{
	do
	{
		cout << "You only have " << distance << " left" << endl;
		cout << "How far will you go? ";
		cin >> bet;

		if (bet < minimum || bet > distance)
		{
			cout << "Invalid bet, please try again." << endl;
			cout << endl;
		}
		else
		{
			cout << "You placed " << bet << " mm for your bet." << endl;
		}
	} while (bet < minimum || bet > distance);
}

//slave deck
void sRound(int& perica, int& bet, int& distance)
{
	int aiChoice = rand() & 4 + 1;
	int userChoice;

	vector <string> sCard;
	sCard.push_back("1. Slave");
	sCard.push_back("2. Civilian");
	sCard.push_back("3. Civilian");
	sCard.push_back("4. Civilian");
	sCard.push_back("5. Civilian");

	cout << "You have the Slave Deck.";
	cout << endl;

	for (unsigned int i = 0; i < sCard.size(); i++)
	{
		cout << sCard[i];
		cout << endl;
	}

	wager(perica, bet, distance);

	cout << "Enter a card: ";
	cin >> userChoice;

	if (userChoice == 1 && aiChoice > 1)
	{
		distance -= bet;
		cout << "You have chosen: " << userChoice << endl;
		cout << "Tonegawa has chosen: " << aiChoice << endl;
		cout << "You lost" << endl;
		cout << "The device will now move " << bet << " closer" << endl;
	}
	else if (userChoice > 1 && aiChoice == 1)
	{
		distance -= bet;
		cout << "You have chosen: " << userChoice << endl;
		cout << "Tonegawa has chosen: " << aiChoice << endl;
		cout << "You lost" << endl;
		cout << "The device will now move " << bet << " closer" << endl;
	}
	else if (userChoice == 1 && aiChoice == 1)
	{
		perica += bet * 500000;
		cout << "You have chosen: " << userChoice << endl;
		cout << "Tonegawa has chosen: " << aiChoice << endl;
		cout << "You won: " << perica;
	}
	else if (userChoice > 1 && aiChoice > 1)
	{
		cout << "Both of you have chosen a Civilian Card" << endl;
	}

	sCard.erase(sCard.begin() + userChoice);

	system("pause");
	system("CLS");
}

//emperor deck
void eRound(int& perica, int& bet, int& distance)
{
	int aiChoice = rand() & 4 + 1;
	int userChoice;

	vector <string> eCard;
	eCard.push_back("1. Emperor");
	eCard.push_back("2. Civilian");
	eCard.push_back("3. Civilian");
	eCard.push_back("4. Civilian");
	eCard.push_back("5. Civilian");

	cout << "You have the Emperor Deck,.";
	cout << endl;


	for (int i = 0; i < eCard.size(); i++)
	{
		cout << eCard[i];
		cout << endl;
	}

	wager(perica, bet, distance);

	cout << "Enter a card: ";
	cin >> userChoice;

	if (userChoice == 1 && aiChoice > 1)
	{
		perica += bet * 100000;
		cout << "You have chosen: " << userChoice << endl;
		cout << "Tonegawa has chosen: " << aiChoice << endl;
		cout << "You won: " << perica << endl;
	}
	else if (userChoice > 1 && aiChoice == 1)
	{
		perica += bet * 100000;
		cout << "You have chosen: " << userChoice << endl;
		cout << "Tonegawa has chosen: " << aiChoice << endl;
		cout << "You won: " << perica << endl;
	}
	else if (userChoice == 1 && aiChoice == 1)
	{
		distance -= bet;
		cout << "You have chosen: " << userChoice << endl;
		cout << "Tonegawa has chosen: " << aiChoice << endl;
		cout << "You lost" << endl;
		cout << "The device will now move " << bet << " closer" << endl;
	}

	else if (userChoice > 1 && aiChoice > 1)
	{
		cout << "Both of you have chosen a Civillian Card" << endl;
	}

	eCard.erase(eCard.begin() + userChoice);

	system("pause");
	system("CLS");
}

//results
void results(int& distance, int& perica)
{
	if (perica >= goal && distance >= 1)
	{
		cout << "Game Over" << endl;
		cout << "Best Ending" << endl;
	}
	else if (perica < goal && distance >= 1)
	{
		cout << "Game Over" << endl;
		cout << "Meh Ending" << endl;
	}
	else if (perica < goal || distance <= 0)
	{
		cout << "Game Over" << endl;
		cout << "BAD ENDING" << endl;
	}

	cout << "Distance Left " << distance << endl;
	cout << "You earned " << perica << " perica" << endl;

	
}

int main()
{
	srand(time(0));

	int distance = 30;
	int bet = 0;
	int perica = 0;
	int round = 1;

	//correct loop and switching
	do
	{
		cout << "Round number[" << round << "]" << endl;
		cout << "You currently have: " << perica << " perica" << endl;
		eRound(perica, bet, distance);
		if (round == 3 || round == 6 || round == 9 || round == 12)
		{
			for (int i = 0; i < 3; i++)
			{
				round++;
				cout << "Round number[" << round << "]" << endl;
				cout << "You currently have: " << perica << " perica" << endl;
				sRound(perica, bet, distance);
			}
		}
		round++;
	} while (round != 21 && distance != 0);

	//printing of results(best, meh, or bad)
	results(distance, perica);

	system("pause");
	system("CLS");
	return 0;
}