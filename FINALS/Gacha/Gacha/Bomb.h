#pragma once
#include "Item.h"
#include <string>
#include <iostream>
using namespace std;

class Bomb : public Item
{
public:
	Bomb(string name, float chance, int damageValue);
	~Bomb();

	void getEffect(Player* player);

private:
	float mChance;
	int mDamageValue;
};